package com.baolh.sampleapp;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.baolh.sampleapp.fragment.AboutFragment;
import com.baolh.sampleapp.fragment.FullScreenMenuFragment;
import com.baolh.sampleapp.fragment.MenuCallbacks;
import com.baolh.sampleapp.fragment.PhotosFragment;
import com.baolh.sampleapp.fragment.PostFragment;
import com.baolh.sampleapp.fragment.VideosFragment;


public class FullScreenMenuActivity extends AppCompatActivity implements MenuCallbacks{

    private Toolbar toolbar;
    private Fragment currentFragment;
    private Menu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_full_screen);
        toolbar = (Toolbar) this.findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        openFragment(new AboutFragment());
    }
    @Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_celebrity, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.menu_toggle:
                if(getSupportFragmentManager().findFragmentByTag("menu")==null) {
                    openMenu();
                }else{
                    closeMenu();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openFragment(Fragment fragment){
        currentFragment = fragment;
        setMainFragment(currentFragment, null);
    }

    private void setMainFragment(Fragment fragment, String tag) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment, tag).commit();
    }

    private void openMenu(){
        getSupportActionBar().setTitle(R.string.toolbar_menu_title);
        setMenuToggleIcon(R.drawable.ic_menu_close);
        FullScreenMenuFragment fullScreenMenuFragment = new FullScreenMenuFragment();
        fullScreenMenuFragment.setMenuCallbacks(this);
        setMainFragment(fullScreenMenuFragment, "menu");
    }

    private void closeMenu(){
        getSupportActionBar().setTitle(R.string.activity_right_drawer_title);
        setMenuToggleIcon(R.drawable.ic_menu_toggle);
        setMainFragment(currentFragment, null);
    }
    private void setMenuToggleIcon(int iconResouceId){
        if(menu!=null) {
            menu.findItem(R.id.menu_toggle).setIcon(iconResouceId);
        }
    }
    @Override
    public void onMenuItemSelected(int position) {
        Fragment fragment = null;
        int toolbarTitleResource = 0;
        switch (position){
            case 0:
                fragment = new AboutFragment();
                toolbarTitleResource = R.string.toolbar_about_title;
                break;
            case 1:
                fragment = new PostFragment();
                toolbarTitleResource = R.string.toolbar_posts_title;
                break;
            case 2:
                fragment = new PhotosFragment();
                toolbarTitleResource = R.string.toolbar_photos_title;
                break;
            case 3:
                fragment = new VideosFragment();
                toolbarTitleResource = R.string.toolbar_videos_title;
                break;
            default:6
                break;
        }
        if(fragment!=null){
            setMenuToggleIcon(R.drawable.ic_menu_toggle);
            openFragment(fragment);
            getSupportActionBar().setTitle(toolbarTitleResource);
        }
    }
}
