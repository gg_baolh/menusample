package com.baolh.sampleapp.fragment;

public interface MenuCallbacks {
    void onMenuItemSelected(int position);
}
