package com.baolh.sampleapp.fragment;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
