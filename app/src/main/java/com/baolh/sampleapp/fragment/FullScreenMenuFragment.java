package com.baolh.sampleapp.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baolh.sampleapp.R;

public class FullScreenMenuFragment extends Fragment implements View.OnClickListener{

	private LinearLayout llMain;
	private TextView tvFragmentName;
	private MenuCallbacks menuCallbacks;

	public void setMenuCallbacks(MenuCallbacks menuCallbacks){
		this.menuCallbacks = menuCallbacks;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fullscreen_menu_fragment_layout, null);

		view.findViewById(R.id.menu_about_button).setOnClickListener(this);
		view.findViewById(R.id.menu_posts_button).setOnClickListener(this);
		view.findViewById(R.id.menu_photos_button).setOnClickListener(this);
		view.findViewById(R.id.menu_videos_button).setOnClickListener(this);
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.menu_about_button:
				menuCallbacks.onMenuItemSelected(0);
				break;
			case R.id.menu_posts_button:
				menuCallbacks.onMenuItemSelected(1);
				break;
			case R.id.menu_photos_button:
				menuCallbacks.onMenuItemSelected(2);
				break;
			case R.id.menu_videos_button:
				menuCallbacks.onMenuItemSelected(3);
				break;
			default:
				break;
		}
	}
}
