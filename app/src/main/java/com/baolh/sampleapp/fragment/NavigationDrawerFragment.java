package com.baolh.sampleapp.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import com.baolh.sampleapp.R;
import com.baolh.sampleapp.widget.NavigationItem;

import java.util.ArrayList;
import java.util.List;

public class NavigationDrawerFragment extends Fragment implements NavigationDrawerCallbacks, OnClickListener {
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";
    private static final String PREFERENCES_FILE = "my_app_settings"; //TODO: change this to your file
    private View mFragmentContainerView;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private boolean mFromSavedInstanceState;
    private int mCurrentSelectedPosition;
    private NavigationDrawerCallbacks mCallbacks;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        view.findViewById(R.id.nav_posts).setOnClickListener(this);
        view.findViewById(R.id.nav_photos).setOnClickListener(this);
        view.findViewById(R.id.nav_videos).setOnClickListener(this);
        view.findViewById(R.id.nav_about).setOnClickListener(this);
        view.findViewById(R.id.nav_about).setOnClickListener(this);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }
    }
    @Override
    public void onStart() {
    	super.onStart();
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallbacks = (NavigationDrawerCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
        }
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return mActionBarDrawerToggle;
    }

    public void setActionBarDrawerToggle(ActionBarDrawerToggle actionBarDrawerToggle) {
        mActionBarDrawerToggle = actionBarDrawerToggle;
    }
    //setup without drawer toggle left side
    public void setup(int fragmentId, DrawerLayout drawerLayout){
        mFragmentContainerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.md_indigo_500));
    }
    //setup with drawer toggle left side
    public void setup( int fragmentId, DrawerLayout drawerLayout, Toolbar toolbar) {
        setup(fragmentId, drawerLayout);
        mActionBarDrawerToggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if (!isAdded()) return;
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!isAdded()) return;
                getActivity().invalidateOptionsMenu();
            }
        };

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mActionBarDrawerToggle.syncState();
            }
        });

        mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);
    }

    public void openDrawer() {
        mDrawerLayout.openDrawer(mFragmentContainerView);
    }

    public void closeDrawer() {
    	if(mDrawerLayout!=null){
    		mDrawerLayout.closeDrawer(mFragmentContainerView);
    	}
    }
    
    @Override
    public void onDetach() {
        super.onDetach();
        mCallbacks = null;
    }

    public void selectItem(int position) {
        mCurrentSelectedPosition = position;
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawer(mFragmentContainerView);
        }
        if (mCallbacks != null) {
            mCallbacks.onNavigationDrawerItemSelected(position);
        }
    }
   
    public boolean isDrawerOpen() {
        return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mActionBarDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        selectItem(position);
    }

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    public void setDrawerLayout(DrawerLayout drawerLayout) {
        mDrawerLayout = drawerLayout;
    }

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.nav_posts:
			mCallbacks.onNavigationDrawerItemSelected(10);
            Toast.makeText(getActivity(),"Posts",Toast.LENGTH_SHORT).show();
            break;
        case R.id.nav_photos:
            Toast.makeText(getActivity(),"Photos",Toast.LENGTH_SHORT).show();
			mCallbacks.onNavigationDrawerItemSelected(11);
			break;
		case R.id.nav_videos:
            Toast.makeText(getActivity(),"Videos",Toast.LENGTH_SHORT).show();
			mCallbacks.onNavigationDrawerItemSelected(12);
			break;
		case R.id.nav_about:
            Toast.makeText(getActivity(),"About",Toast.LENGTH_SHORT).show();
			mCallbacks.onNavigationDrawerItemSelected(13);
			break;
		default:
			break;
		}
	}
}
