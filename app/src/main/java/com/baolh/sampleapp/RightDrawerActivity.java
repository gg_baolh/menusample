package com.baolh.sampleapp;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.baolh.sampleapp.fragment.NavigationDrawerCallbacks;
import com.baolh.sampleapp.fragment.NavigationDrawerFragment;


public class RightDrawerActivity extends AppCompatActivity implements NavigationDrawerCallbacks{

    private Toolbar toolbar;
    private NavigationDrawerFragment navigationDrawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_right_drawer);
        toolbar = (Toolbar) this.findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        navigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
    }
    @Override
    protected void onStart() {
        super.onStart();
        navigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer_layout));
        navigationDrawerFragment.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_celebrity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.menu_toggle:
                if(!navigationDrawerFragment.isDrawerOpen()) {
                    getSupportActionBar().setTitle(R.string.toolbar_menu_title);
                    item.setIcon(R.drawable.ic_menu_close);
                    navigationDrawerFragment.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
                }else{
                    getSupportActionBar().setTitle(R.string.activity_right_drawer_title);
                    item.setIcon(R.drawable.ic_menu_toggle);
                    navigationDrawerFragment.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Log.d("right-drawer", "On drawer callback");
    }
}
