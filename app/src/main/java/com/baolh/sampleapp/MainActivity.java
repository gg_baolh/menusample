package com.baolh.sampleapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.baolh.sampleapp.fragment.FullScreenMenuFragment;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) this.findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        Button openLeftDrawer1Button = (Button)this.findViewById(R.id.btn_open_left_drawer_1_activity);
        openLeftDrawer1Button.setOnClickListener(this);
        Button openLeftDrawer2Button = (Button)this.findViewById(R.id.btn_open_left_drawer_2_activity);
        openLeftDrawer2Button.setOnClickListener(this);
        Button openRightDrawerButton = (Button)this.findViewById(R.id.btn_open_right_drawer_activity);
        openRightDrawerButton.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btn_open_left_drawer_1_activity:
                intent = new Intent(MainActivity.this, LeftDrawer1Activity.class);
                this.startActivity(intent);
                break;
            case R.id.btn_open_left_drawer_2_activity:
                intent = new Intent(MainActivity.this, LeftDrawer2Activity.class);
                this.startActivity(intent);
                break;
            case R.id.btn_open_right_drawer_activity:
                intent = new Intent(MainActivity.this, FullScreenMenuActivity.class);
                this.startActivity(intent);
                break;
            default:
                break;
        }
    }
}
