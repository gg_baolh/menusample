package com.baolh.sampleapp;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.baolh.sampleapp.R;
import com.baolh.sampleapp.fragment.NavigationDrawerCallbacks;
import com.baolh.sampleapp.fragment.NavigationDrawerFragment;


public class LeftDrawer1Activity extends AppCompatActivity implements NavigationDrawerCallbacks{
    private NavigationDrawerFragment navigationDrawerFragment;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_left_drawer_1);
        toolbar = (Toolbar) this.findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        navigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager().findFragmentById(R.id.fragment_drawer);
    }

    @Override
    protected void onStart() {
        super.onStart();
        navigationDrawerFragment.setup(R.id.fragment_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        navigationDrawerFragment.getDrawerLayout().setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        if (navigationDrawerFragment.isDrawerOpen())
            navigationDrawerFragment.closeDrawer();
        else
            super.onBackPressed();
    }
    @Override
    public void onNavigationDrawerItemSelected(int position) {
        Log.d("left-drawer", "On drawer callback");
    }
}
